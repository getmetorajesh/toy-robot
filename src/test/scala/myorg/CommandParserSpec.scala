package myorg

import cats.data.Validated.Valid
import myorg.Action.PLACE
import org.scalatest.{FlatSpec, Matchers}


/**
  */
class CommandParserSpec extends FlatSpec with Matchers {
  "Given a valid raw string 'PLACE 0,2,NORTH' to Parse" should "return valid command PLACE(Position(0,2), NORTH)" in {
    val input = "PLACE 0,2,NORTH"
    val command = CommandParser.parse(input)
    command should be(Valid(PLACE(Position(0, 2, NORTH))))
  }
}
