package myorg

import myorg.Action.MOVE
import org.scalatest.{FlatSpec, Matchers}

/**
  */
class ApplicationSpec extends FlatSpec with Matchers {
  "Given a Move command for a Robot(0,1,WEST)" should "return a new Robot(0,2,WEST)" in {
    val robot = Robot(Position(1, 1, WEST))
    val newRobot = Application.commandHandler(robot, MOVE)
    newRobot.position.x should be(0)
  }

}
