package myorg

import org.scalatest.{FlatSpec, Matchers}

/**
  */
class RobotSpec extends FlatSpec with Matchers {
  "MOVE command when Robot is Facing North" should "increment y position of the Robot" in {
    val robot = Robot(Position(0, 0, NORTH))
    val newRobot = Robot.move(robot)
    newRobot.position.x should be(0)
    newRobot.position.y should be(1)
  }

  "LEFT command when Robot is Facing East (1,1)" should "not change position but change facing to NORTH" in {
    val robot = Robot(Position(1, 1, EAST))
    val newRobot = Robot.left(robot)
    newRobot.position should be(Position(1, 1, NORTH))
  }

  "MOVE command when Robot is Facing SOUTH (1,1)" should "change position to EAST" in {
    val robot = Robot(Position(1, 1, SOUTH))
    val newRobot = Robot.move(robot)
    newRobot.position should be(Position(1, 0, SOUTH))
  }

  "MOVE command when Robot is Facing WEST (1,1)" should "change position to EAST" in {
    val robot = Robot(Position(1, 1, WEST))
    val newRobot = Robot.move(robot)
    newRobot.position should be(Position(0, 1, WEST))
  }

  "RIGHT command when Robot is Facing NORTH (3,1)" should "not change position but change facing to EAST" in {
    val robot = Robot(Position(3, 1, NORTH))
    val newRobot = Robot.right(robot)
    newRobot.position.x should be(3)
    newRobot.position.y should be(1)
    newRobot.position.facing should be(EAST)
  }

  "REPORT command" should "return string with the robots precise position" in {
    val robot = new Robot(Position(0, 3, WEST))
    val report = Robot.report(robot)
    report should be("Output: 0, 3, WEST")
  }
}
