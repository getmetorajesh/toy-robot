package myorg

import org.scalatest.{FlatSpec, Matchers}

/**
  */
class TableTopSpec extends FlatSpec with Matchers {
  "Given valid position input" should "return a valid Position" in {
    val result = TableTop.validatePosition(0, 0, "WEST")
    result.isValid should be(true)
  }

  "Given invalid position input" should "return a valid Position" in {
    val result = TableTop.validatePosition(7, 0, "WEST")
    result.isValid should be(false)
  }
}
