package myorg

import cats.data.Validated.Valid
import cats.data.ValidatedNel
import cats.implicits._
import myorg.Action._
import TableTop._


/**
  */
object CommandParser {
  type ValidationResult[A] = ValidatedNel[String, A]

  /**
    * parse raw input from cmd line
    * @param input
    * @return
    */
  def parse(input: String): ValidationResult[ACTION] = {
    val pattern = """(\w{4,6}\s?)(((\d{1})\,\s?(\d{1})\,\s?(\w{4,5}))?)""".r
    val commands = Map("MOVE" -> MOVE, "LEFT" -> LEFT, "RIGHT"-> RIGHT, "REPORT"-> REPORT)
    return input match {
      case cmd: String if commands.keys.toSeq.contains(cmd) => commands(cmd).validNel
      case pattern(cmd, _, _, xAxis: String, yAxis: String, facing: String)
        if cmd.trim == "PLACE" => {
        // safe to use toInt as regex takes care of it
          validatePosition(xAxis.toInt, yAxis.toInt, facing) match {
            case Valid(p:Position) => PLACE(p).validNel
            case _ => "".invalidNel
          }
        }
      case _ => s"${input} is an invalid command".invalidNel
    }
  }

  /**
    * Validate the position. i,e prevent from falling, avoid invalid inp
    * @param x
    * @param y
    * @param facing
    * @return
    */
  def validatePosition(x: Int,
                       y: Int,
                       facing: String): ValidationResult[Position] = {
    (validateRange("x", x), validateRange("y", y), validateFacing(facing)) mapN Position.apply
  }

}
