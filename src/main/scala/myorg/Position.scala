package myorg

/**
  */
sealed trait FACING
case object NORTH extends FACING
case object SOUTH extends FACING
case object EAST extends FACING
case object WEST extends FACING
case class Position(x: Int, y: Int, facing: FACING)

