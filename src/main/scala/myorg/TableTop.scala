package myorg

import cats.data.ValidatedNel
import cats.implicits._

/**
  */
object TableTop {

  val tableRange: Range.Inclusive = 0 to 4
  type ValidationResult[A] = ValidatedNel[String, A]

  /**
    * vaidates the robot position to prevent from going out of bounds
    * @param x
    * @param y
    * @param facing
    * @return
    */
  def validatePosition(x: Int,
                       y: Int,
                       facing: String): ValidationResult[Position] = {
    (validateRange("x", x), validateRange("y", y), validateFacing(facing)) mapN Position.apply
  }

  def validateRange(axis: String, value: Int): ValidationResult[Int] =
    tableRange contains value match {
      case true => value.validNel
      case _ => s"$axis($value) axis is invalid".invalidNel
    }

  def validateFacing(facing: String): ValidationResult[FACING] = {
    facing match {
      case "NORTH" => NORTH.validNel
      case "SOUTH" => SOUTH.validNel
      case "EAST" => EAST.validNel
      case "WEST" => WEST.validNel
      case _ => "Invalid Direction".invalidNel
    }
  }

}
