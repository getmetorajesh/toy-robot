package myorg

import cats.data.Validated.Valid
import myorg.TableTop.ValidationResult

/**
  */
case class Robot(position: Position) {}

object Robot {

  /**
    * Will rotate the robot 90 deg without moving its position
    *
    * @param robot
    * @return Robot
    */
  def left(robot: Robot): Robot = {
    val newFacing = robot.position.facing match {
      case NORTH => WEST
      case SOUTH => EAST
      case EAST => NORTH
      case WEST => SOUTH
      case _ => robot.position.facing // same facing no changes
    }
    val newPosition = robot.position.copy(facing = newFacing)
    Robot(newPosition)
  }

  /**
    * Will rotate the robot 90 deg without moving its position
    * @param robot
    * @return
    */
  def right(robot: Robot): Robot = {
    val newFacing = robot.position.facing match {
      case NORTH => EAST
      case SOUTH => WEST
      case EAST => SOUTH
      case WEST => NORTH
    }
    val newPosition = robot.position.copy(facing = newFacing)
    Robot(newPosition)
  }

  /**
    * Move the robot safely one step at a time
    *
    * @param robot
    * @return
    */
  def move(robot: Robot): Robot = {
    val newPosition = robot.position.facing match {
      case NORTH =>
        Position(robot.position.x, robot.position.y + 1, robot.position.facing)
      case SOUTH =>
        Position(robot.position.x, robot.position.y - 1, robot.position.facing)
      case EAST =>
        Position(robot.position.x + 1, robot.position.y, robot.position.facing)
      case WEST =>
        Position(robot.position.x - 1, robot.position.y, robot.position.facing)
    }
    val validPosition: ValidationResult[Position] = TableTop.validatePosition(
      newPosition.x,
      newPosition.y,
      newPosition.facing.toString())
    validPosition match {
      case Valid(position) => Robot(position)
      case _ => robot
    }
  }

  def report(robot: Robot): String = {
    s"Output: ${robot.position.x}, ${robot.position.y}, ${robot.position.facing}"
  }

}