package myorg


/**
  */
object Action {

  trait ACTION
  case class PLACE(position: Position) extends ACTION
  case object MOVE extends ACTION
  case object REPORT extends ACTION
  case object LEFT extends ACTION
  case object RIGHT extends ACTION

}
