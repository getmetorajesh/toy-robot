package myorg

import cats.data.Validated.{Invalid, Valid}
import myorg.Action._
import myorg.Robot._
import myorg.TableTop.ValidationResult

import scala.io.StdIn

object Application {
  def main(args: Array[String]): Unit = {
    val help = s""" **** TOY ROBOT ****
      PLACE X, Y, F(NORTH, SOUTH, WEST, EAST)
      MOVE
      LEFT
      RIGHT
      REPORT
      *********
      """
    println(help)
    commandListener(None)
  }

  /**
    * Listener for the user input
    * @param robot
    */
  def commandListener(robot: Option[Robot]): Unit = {
    val rawCommand = StdIn.readLine()
    val command: ValidationResult[ACTION] = CommandParser.parse(rawCommand)
    val newRobot: Option[Robot] = command match {
      case Valid(action) =>
        action match {
          case PLACE(position) => Some(Robot(position))
          case _ =>
            // None here means the robot is not placed in the table top
            if (robot.isDefined) Some(commandHandler(robot.get, action))
            else None
        }
      case Invalid(x) =>
        println(x.head)
        robot
    }
    commandListener(newRobot)
  }

  /**
    * Handler that executes the command
    * @param robot
    * @param cmd
    * @return
    */
  def commandHandler(robot: Robot, cmd: ACTION): Robot = {
    cmd match {
      case MOVE => move(robot)
      case LEFT => left(robot)
      case RIGHT => right(robot)
      case REPORT =>
        val position = report(robot: Robot)
        println(position)
        robot
    }
  }
}
