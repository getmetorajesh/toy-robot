import sbt._

object Dependencies {
  val catsVersion = "1.1.0"
  val scalaTestVersion = "3.0.5"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVersion
  lazy val catsCore = "org.typelevel" %% "cats-core" % catsVersion
}
